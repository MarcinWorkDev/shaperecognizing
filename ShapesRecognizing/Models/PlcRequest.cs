﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShapesRecognizing.Models
{
    public class PlcRequest
    {
        public int Width { get; set; }

        public int Height { get; set; }
        
        public int Variant { get; set; }
        
        public int Rotation { get; set; }
    }
}
