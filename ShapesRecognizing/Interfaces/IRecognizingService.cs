﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ShapesRecognizing.Interfaces
{
    public interface IRecognizingService
    {
        void Run();
    }
}
