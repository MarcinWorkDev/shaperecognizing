﻿using Newtonsoft.Json;
using ShapesRecognizing.Interfaces;
using ShapesRecognizing.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShapesRecognizing.Services
{
    public class PlcConnectorMock : IPlcConnector
    {
        public void SendToPlc(PlcRequest plcRequest)
        {
            Console.WriteLine($"PLC Request: {JsonConvert.SerializeObject(plcRequest, Formatting.None)}");
        }
    }
}
