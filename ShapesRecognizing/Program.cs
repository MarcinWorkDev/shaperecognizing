﻿using ShapesRecognizing.Interfaces;
using ShapesRecognizing.Services;
using System;

namespace ShapesRecognizing
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Hello World!");
            Console.WriteLine();

            var filePath = @"formatki.csv";

            ICameraConnector cameraConnector = new CameraConnectorMock(filePath);
            IPlcConnector plcConnector = new PlcConnectorMock();

            var recognizingService = new RecognizingService(cameraConnector, plcConnector, filePath);
            recognizingService.Run();

            Console.ReadKey();
        }
    }
}
