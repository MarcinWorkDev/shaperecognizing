﻿using Newtonsoft.Json;
using ShapesRecognizing.Interfaces;
using ShapesRecognizing.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace ShapesRecognizing.Services
{
    public class RecognizingService : IRecognizingService
    {
        private readonly ICameraConnector _cameraConnector;
        private readonly IPlcConnector _plcConnector;
        private readonly string _filePath;

        public RecognizingService(ICameraConnector cameraConnector, IPlcConnector plcConnector, string filePath)
        {
            _cameraConnector = cameraConnector;
            _plcConnector = plcConnector;
            _filePath = filePath;
        }

        public void Run()
        {
            var shapes = this.ReadFile();
            var counter = _cameraConnector.ReadMeasureItemCount();

            while (shapes.Count(x => x.Volume > 0) > 0)
            {
                Console.WriteLine();

                var counterCurrent = _cameraConnector.ReadMeasureItemCount();
                if (counterCurrent > counter)
                {
                    var image = _cameraConnector.ReadImage();

                    // Przerwij procesowanie jeżeli jakość pomiaru jest zła
                    if (image.Judgement == 1)
                    {
                        Console.WriteLine($"Liczba pozostałych obiektów: {shapes.Count(x => x.Volume > 0)}");
                        Console.WriteLine();
                        continue;
                    }

                    int errorMax = 5;
                    int error1 = errorMax;
                    int error2 = errorMax;
                    int row1 = 0;
                    int row2 = 0;
                    
                    // Obiekt ulozony prawidlowo
                    for (int row = 0; row < shapes.Count(); row++)
                    {
                        // pobranie aktualnego rekordu
                        var shapeCurrent = shapes[row];

                        // sprawdzenie czy liczba sztuk > 0
                        if (shapeCurrent.Volume > 0)
                        {
                            // Obliczenie blędu rozpoznania
                            var error = Math.Abs(image.Width - shapeCurrent.Width) + Math.Abs(image.Height - shapeCurrent.Height);

                            // Sprawdzenie wartości błędu
                            if (error < error1)
                            {
                                error1 = error;
                                row1 = row;
                            }
                        }
                    }

                    // Obiekt obrócony
                    for (int row = 0; row < shapes.Count(); row++)
                    {
                        // pobranie aktualnego rekordu
                        var shapeCurrent = shapes[row];

                        // sprawdzenie czy liczba sztuk > 0
                        if (shapeCurrent.Variant > 0)
                        {
                            // Obliczenie blędu rozpoznania
                            var error = Math.Abs(image.Width - shapeCurrent.Height) + Math.Abs(image.Height - shapeCurrent.Width);

                            // Sprawdzenie wartości błędu
                            if (error < error2)
                            {
                                error2 = error;
                                row2 = row;
                            }
                        }
                    }
                    
                    var shape = new PlcRequest();
                    if (error1 <= error2)
                    {
                        shape = new PlcRequest()
                        {
                            Width = shapes[row1].Width,
                            Height = shapes[row1].Height,
                            Variant = shapes[row1].Variant,
                            Rotation = 0
                        };
                        
                        shapes[row1].Volume -= 1;
                    }
                    else
                    {
                        shape = new PlcRequest()
                        {
                            Width = shapes[row2].Width,
                            Height = shapes[row2].Height,
                            Variant = shapes[row2].Variant,
                            Rotation = 1
                        };

                        shapes[row2].Volume -= 1;
                    }

                    if (error1 >= errorMax && error2 >= errorMax)
                    {
                        Console.WriteLine($"Zbyt duży błąd!");
                        shape = new PlcRequest()
                        {
                            Width = 0,
                            Height = 0,
                            Variant = 0,
                            Rotation = 0
                        };
                    }

                    // Wyślij do PLC
                    _plcConnector.SendToPlc(shape);

                    // Ustaw counter
                    counter = counterCurrent;

                    // Zaktualizuj plik
                    this.SaveFile(shapes);
                }
                
                Console.WriteLine($"Liczba pozostałych obiektów: {shapes.Count(x => x.Volume > 0)}");
                Console.WriteLine();

                Thread.Sleep(200);
            }
        }

        private List<Shape> ReadFile()
        {
            Console.WriteLine("Wczytanie pliku ...START");

            var shapes = new List<Shape>();
            var lines = File.ReadAllLines(_filePath).ToList();
            for (var row = 0; row < lines.Count(); row++)
            {
                var pars = lines[row].Split(';', 4);

                Console.WriteLine($"[{row}] Width: {pars[0]}, Height: {pars[1]}, Volume: {pars[2]}, Variant: {pars[3]}");

                shapes.Add(new Shape
                {
                    Width = int.Parse(pars[0]),
                    Height = int.Parse(pars[1]),
                    Volume = int.Parse(pars[2]),
                    Variant = int.Parse(pars[3])
                });
            }
            
            Console.WriteLine("Wczytanie pliku ...END");

            return shapes;
        }

        private void SaveFile(List<Shape> shapes)
        {
            File.WriteAllLines(_filePath, shapes.Select(x => $"{x.Width};{x.Height};{x.Volume};{x.Variant}"));
        }
    }
}
