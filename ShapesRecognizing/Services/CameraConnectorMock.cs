﻿using Newtonsoft.Json;
using ShapesRecognizing.Interfaces;
using ShapesRecognizing.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ShapesRecognizing.Services
{
    public class CameraConnectorMock : ICameraConnector
    {
        private readonly Random _rn;
        private readonly string _filePath;
        private int _counter = 0;

        public CameraConnectorMock(string filePath)
        {
            _filePath = filePath;
            _rn = new Random();
        }

        public CameraScanResult ReadImage()
        {
            

            var shapes = new List<Shape>();
            var lines = File.ReadAllLines(_filePath).ToList();
            for (var line = 0; line < lines.Count(); line++)
            {
                var pars = lines[line].Split(';', 4);

                if (int.Parse(pars[2]) > 0)
                {
                    shapes.Add(new Shape
                    {
                        Width = int.Parse(pars[0]),
                        Height = int.Parse(pars[1]),
                        Volume = int.Parse(pars[2]),
                        Variant = int.Parse(pars[3])
                    });
                }
            }

            var result = new CameraScanResult();
            var invert = _rn.Next(1, 3);
            var row = _rn.Next(0, shapes.Count());

            if (invert == 1)
            {
                result = new CameraScanResult()
                {
                    Width = shapes[row].Width + _rn.Next(0, 4),
                    Height = shapes[row].Height + _rn.Next(0, 4),
                    Judgement = _rn.Next(0, 2)
                };
            } else
            {
                result = new CameraScanResult()
                {
                    Width = shapes[row].Height + _rn.Next(0, 4),
                    Height = shapes[row].Width + _rn.Next(0, 4),
                    Judgement = _rn.Next(0, 2)
                };
            }
            
            Console.WriteLine($"Camera Response: {JsonConvert.SerializeObject(result, Formatting.None)}");

            return result;
        }

        public int ReadMeasureItemCount()
        {
            return _counter++;
        }
    }
}
