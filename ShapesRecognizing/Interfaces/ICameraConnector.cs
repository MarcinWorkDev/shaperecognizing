﻿using ShapesRecognizing.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShapesRecognizing.Interfaces
{
    public interface ICameraConnector
    {
        int ReadMeasureItemCount();

        CameraScanResult ReadImage();
    }
}
