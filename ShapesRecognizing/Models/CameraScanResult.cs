﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShapesRecognizing.Models
{
    public class CameraScanResult
    {
        /// <summary>
        /// Szerokość obiektu
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Wysokość obiektu
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Ocena pomiaru
        /// </summary>
        public int Judgement { get; set; }
    }
}
