﻿using ShapesRecognizing.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShapesRecognizing.Interfaces
{
    public interface IPlcConnector
    {
        void SendToPlc(PlcRequest plcRequest);
    }
}
